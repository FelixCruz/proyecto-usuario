# from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from users.serializers.user_serializers import UserSerializer,UserListar,UserSerializerActualizar
from users.models.users import Users


class UserCreate(APIView):

    def post(self, request):
        serializador = UserSerializer(data=request.data)
        serializador.is_valid()
        serializador.save()
        return Response({}, status=201)

    def get(self, request):
        queryset = Users.objects.all().order_by('id')
        serializador = UserListar(queryset, many=True)
        return Response(serializador.data)

    def put(self, request):
        name = request.data['first_name']
        queryset = Users.objects.get(first_name=name)   
        serializador = UserSerializer(queryset,data=request.data)
        serializador.is_valid()
        serializador.save()
        return Response(serializador.data,status=status.HTTP_204_NO_CONTENT)
    
    def patch(self,request,iduser):
        queryset = Users.objects.get(id=iduser)
        serializador = UserSerializer(queryset,data=request.data,partial=True)
        serializador.is_valid()
        serializador.save()
        return Response(serializador.data,status=status.HTTP_204_NO_CONTENT)
    
    def delete(self,request,iduser):
        queryset = Users.objects.get(id=iduser)
        serializador = UserSerializer(queryset)
        queryset.delete()
        return Response(serializador.data)

class UserListarId(APIView):
    def get(self,request,iduser):
        queryset = Users.objects.filter(id=iduser)
        serializador = UserListar(queryset,many=True)
        return Response(serializador.data)

class Actualizar(APIView):

    def patch(self,request,iduser):
        queryset = Users.objects.get(id=iduser)
        serializador = UserSerializerActualizar(queryset,data=request.data,partial=True)
        serializador.is_valid()
        serializador.save()
        return Response(serializador.data,status=status.HTTP_204_NO_CONTENT)