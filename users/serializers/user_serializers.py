from rest_framework import serializers
from users.models.users import Users
from django.contrib.auth import password_validation, authenticate


class UserSerializer(serializers.Serializer):

    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    mobil_phone = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    gender = serializers.CharField(required=False)
    city_id = serializers.IntegerField(required=False)
    profile_id = serializers.IntegerField(required=False)
    nid_id = serializers.IntegerField(required=False)
    nids = serializers.CharField(required=False)
    sms_code = serializers.TimeField(required=False)
    activate = serializers.BooleanField(required=False)

    def create(self, data):
        profile_id = data['profile_id']
        del data['profile_id']
        users = Users.objects.create(**data)
        users.profile.add(profile_id)
        users.set_password(data['password'])
        users.save()
        return users

    def update(self, instance, data):
        instance.first_name = data.get('first_name', instance.first_name)
        instance.last_name = data.get('last_name', instance.last_name)
        instance.email = data.get('email', instance.email)
        instance.mobil_phone = data.get('mobil_phone', instance.mobil_phone)
        instance.set_password(data.get('password',instance.password))
        instance.save()
        return instance


class UserListar(serializers.ModelSerializer):
    class Meta:
        model = Users
        # fields = ['id','first_name','last_name','nids','gender','profile_id','password','mobil_phone']
        #exclude = ['edad']
        fields = '__all__'
        depth = 1


class UserSerializerActualizar(serializers.Serializer):

    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    mobil_phone = serializers.CharField(required=False)
    password = serializers.CharField(required=False)

    def update(self, instance, data):
        instance.first_name = data.get('first_name', instance.first_name)
        instance.last_name = data.get('last_name', instance.last_name)
        instance.email = data.get('email', instance.email)
        instance.mobil_phone = data.get('mobil_phone', instance.mobil_phone)
        # instance.password = data.get('password', instance.password)
        instance.set_password(data.get('password',instance.password))
        # instance.password= set_password(data.get('password',instance.password))
        # self.object.set_password(serializer.data.get("new_password"))
        instance.save()
        return instance

#Clase para solo resetear el password
# class R_pwd(serializers.Serializer):

#     id_user=serializers.IntegerField()
#     password=serializers.CharField()

#     def create(self,data):
#         user_id=data['id_user']
#         pwd=data['password']

#         user=Users.objects.filter(id=user_id)
#         user.set_password(pwd)

#         return user
