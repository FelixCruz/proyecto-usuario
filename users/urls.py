from users.views import views as user_views
from django.urls import path

urlpatterns = [
    path(
        route='<int:iduser>',
        view=user_views.UserCreate.as_view(),
        name='userlistar'
    ),
    path(
        route='datos/<int:iduser>',
        view=user_views.UserListarId.as_view(),
        name='userlistar'
    ),
    path(
        route='Actualizar/<int:iduser>',
        view=user_views.Actualizar.as_view(),
        name='actualizarid'
    )
]
