from django.db import models
from users.models.profileType import ProfileType
from users.models.cities import City
from django.core.validators import MinLengthValidator
from django.contrib.auth.models import AbstractUser

class Users(AbstractUser):
# class Users(models.Model):
    username = None
    last_login = None
    is_superuser = None
    is_staff = None
    is_active = None
    date_joined = None
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    email = models.CharField(max_length=128, unique=True)
    mobil_phone = models.CharField(max_length=11)
    password = models.TextField(max_length=64, validators=[
        MinLengthValidator(11)])
    address = models.TextField(max_length=60, blank=True)
    gender = models.CharField(blank=True, max_length=2, null=True)
    city = models.ForeignKey(
        City, on_delete=models.CASCADE, null=True, blank=True)
    profile = models.ManyToManyField(ProfileType, db_table='users_profile')
    nid_id = models.IntegerField()
    nids = models.CharField(max_length=20)
    sms_code = models.CharField(blank=True, max_length=4)
    activate = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    REQUIRED_FIELDS = ['first_name', 'last_name']
    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'

    class Meta:
        db_table = 'user'
