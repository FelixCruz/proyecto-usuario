from django.db import models


class City(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=10)
    latitude = models.FloatField()
    longitude =models.FloatField()
    country_state = models.IntegerField()
    activate = models.BooleanField(default=True) 
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    class Meta:
        db_table = 'cities'
