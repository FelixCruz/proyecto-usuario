from django.db import models

class ProfileType(models.Model):

    name = models.CharField(max_length=64)
    description = models.TextField(max_length=64)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)

    class Meta:
        db_table = 'profyle_type'
