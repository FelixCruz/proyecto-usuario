from django.db import models
from users.models.users import Users

class Reset_Password(models.Model):

    token = models.TextField(max_length=64)
    user = models.ForeignKey(Users,on_delete=models.CASCADE,null=True,blank=True)
    activate = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)

    class Meta:
        db_table = 'reset_password'